# Gothic 2 Online - Simple Anty-Cheat
This is simple anty-cheat, that protects the server from unexpected change strength, dexterity, health, maxhealth, mana and maxmana.

### INSTALL STEP BY STEP:

1. Copy and paste all directory and files to main server-files folder.

2. Import configuration from **ac.xml** in your config.xml(before your gamemode scripts!).

3. Add to onInit (server-side):
**initAntyCheat(true);**

4. Add to onPlayerDisconnect(pid, reason) SERVER-SIDE:
**clearACStructure(pid);**

**Requirements:**

packets.nut client/server side from default G2O scripts (you must include it before AC)

### Wiki:
- [G2O_SIMPLE_AC Wiki](https://gitlab.com/Profesores/G2O_Simple_AC/wikis/home)