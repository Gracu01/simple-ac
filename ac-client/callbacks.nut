/*
AntyCheat by Profesores
Gothic 2 Online
Last Edit: 2018-02-28

Callbacks - clientside
*/

addEventHandler("onUseItem", function(instance, amount, hand)
{
	if(instance.find("ITPO_") != null)
		callServerFunc("registerUseItem", heroId, instance, amount);
	if(instance.find("ITFO_") != null)
		callServerFunc("registerUseItem", heroId, instance, amount);
	if(instance.find("ITPL_") != null)
		callServerFunc("registerUseItem", heroId, instance, amount);
	if(instance == "ITFOMUTTONRAW" || instance == "ITFOMUTTON")
		callServerFunc("registerUseItem", heroId, instance, amount);
});