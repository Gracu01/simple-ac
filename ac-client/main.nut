/*
AntyCheat by Profesores
Gothic 2 Online
Last Edit: 2018-02-28

Main - CLIENT
*/

function checkValues(str, dex, mana, maxmana)
{
	if(getPlayerStrength(heroId) > str)
		callServerFunc("onPlayerUseCheats", heroId, "strength", getPlayerStrength(heroId), str);
	if(getPlayerDexterity(heroId) > dex)
		callServerFunc("onPlayerUseCheats", heroId, "dexterity", getPlayerDexterity(heroId), dex);
	if(getPlayerMana(heroId) > mana)
		callServerFunc("onPlayerUseCheats", heroId, "mana", getPlayerMana(heroId), mana);
	if(getPlayerMaxMana(heroId) > maxmana)
		callServerFunc("onPlayerUseCheats", heroId, "maxmana", getPlayerMaxMana(heroId), mana);
};

function equipArmor(pid, instance)
{
	callServerFunc("equipArmor", pid, instance);
};

function equipMeleeWeapon(pid, instance)
{
	callServerFunc("equipMeleeWeapon", pid, instance);
};

function equipRangedWeapon(pid, instance)
{
	callServerFunc("equipRangedWeapon", pid, instance);
};

function clearInventory()
{
	callServerFunc("clearInventory", heroId);
	
	_clearInventory();
};