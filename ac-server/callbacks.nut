/*
AntyCheat by Profesores
Gothic 2 Online
Last Edit: 2018-02-20

Callbacks
*/

function onPlayerUseCheats(pid, category, newvalue, oldvalue) //this  is only example, you can  ban cheaters here
{
	sendMessageToAll(255, 0, 0, getPlayerName(pid) + " change value " + category + " from " + oldvalue + " to " + newvalue);
	
	callEvent("onPlayerUseCheats", pid, category, newvalue, oldvalue);
};

addEventHandler("onPlayerDropItem", function(pid, item_ground)
{
	registerDropItem(pid, item_ground);
});

addEventHandler("onPlayerTakeItem", function(pid, item_ground)
{
	registerTakeItem(pid, item_ground);
});