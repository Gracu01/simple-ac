/*
AntyCheat by Profesores
Gothic 2 Online
Last Edit: 2018-03-20

Main
*/

local player = {};
local ac_status = false;

function initAntyCheat(value)
{
	if(typeof value == "boolean")
		ac_status = value;
		
	for(local i = 0; i < getMaxSlots(); ++i)
	{
		player[i] <- {};
		player[i].hp <- 40;
		player[i].maxhp <- 40;
		
		player[i].items <- [];
	}
	
	setTimer(checkValues, 3000, 0);
	addEvent("onPlayerUseCheats");
	
	print("\n[Anty-Cheat by Profesores has been successful loaded!]");
	print("[Your server are protected from now :D]");
};

function clearACStructure(pid)
{
	player[pid].hp = 40;
	player[pid].maxhp = 40;
	
	player[pid].items.clear();
};

function checkValues()
{
	for(local i = 0; i < getMaxSlots(); ++i)
	{
		if(isPlayerConnected(i) == true)
		{
			if(_getPlayerHealth(i) > getPlayerHealth(i))
				onPlayerUseCheats(i, "health", _getPlayerHealth(i), getPlayerHealth(i));
			if(_getPlayerMaxHealth(i) > getPlayerMaxHealth(i))
				onPlayerUseCheats(i, "maxheath", _getPlayerMaxHealth(i), getPlayerMaxHealth(i));
			
			callClientFunc(i, "checkValues", getPlayerStrength(i), getPlayerDexterity(i), getPlayerMana(i), getPlayerMaxMana(i));
		}
	}
};

function setPlayerHealth(pid, hp)
{
	player[pid].hp = hp;
	_setPlayerHealth(pid, hp);
};

function setPlayerMaxHealth(pid, maxhp)
{
	player[pid].maxhp = maxhp;
	_setPlayerMaxHealth(pid, maxhp);
};

function getPlayerHealth(pid)
{
	return player[pid].hp;
};

function getPlayerMaxHealth(pid)
{
	return player[pid].maxhp;
};

//others
function giveItem(pid, instance, amount)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == instance)
		{
			v[1] += amount;
			_giveItem(pid, instance, amount);
			
			return true;
		};
	};
	
	player[pid].items.push([instance, amount]);
	
	_giveItem(pid, instance, amount);
};

function removeItem(pid, instance, amount)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == instance)
			if(v[1] <= amount)
				player[pid].items.remove(k);
			else
				v[1] -= amount;
	};
	
	_removeItem(pid, instance, amount);
};

function equipArmor(pid, instance)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == instance)
		{
			v[1] += 1;
			_equipArmor(pid, instance);
			
			return true;
		};
	};
	
	player[pid].items.push([instance, 1]);
	
	_equipArmor(pid, instance);
};

function equipMeleeWeapon(pid, instance)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == instance)
		{
			v[1] += 1;
			_equipMeleeWeapon(pid, instance);
			
			return true;
		};
	};
	
	player[pid].items.push([instance, 1]);
	
	_equipMeleeWeapon(pid, instance);
};

function equipRangedWeapon(pid, instance)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == instance)
		{
			v[1] += 1;
			_equipRangedWeapon(pid, instance);
			
			return true;
		};
	};
	
	player[pid].items.push([instance, 1]);
	
	_equipRangedWeapon(pid, instance);
};

function registerTakeItem(pid, item_ground)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == Items.id(item_ground.instance))
		{
			v[1] += item_ground.amount;
			
			return true;
		};
	};
	
	player[pid].items.push([Items.id(item_ground.instance), item_ground.amount]);
};

function registerDropItem(pid, item_ground)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == Items.id(item_ground.instance))
		{
			if(item_ground.amount > v[1])
			{
				_giveItem(pid, Items.id(item_ground.instance), v[1]);
				ItemsGround.destroy(item_ground.id);
				
				return true;
			}
			else
			{
				v[1] -= item_ground.amount;
				
				if(v[1] <= 0)
					player[pid].items.remove(k);
					
				return true;
			};
		};
	};
	
	ItemsGround.destroy(item_ground.id);
};

function registerUseItem(pid, instance, amount)
{
	foreach(k, v in player[pid].items)
	{
		if(v[0] == Items.id(instance))
		{
			v[1] -= amount;
			
			if(v[1] <= 0)
				player[pid].items.remove(k);
		};
	};
};

function getEquipment(pid)
{
	return player[pid].items;
};

function clearInventory(pid)
{
	player[pid].items.clear();
};